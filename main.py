import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt


def r_max(beta=0.01):
    k = 6.0e-6

    alpha = 1.079  # alpha Fig. 1 key (a)
    mc2 = 510.998  # electron mass in KeV

    # maximum range of delta-rays (Eq. 6-7)
    W = 2.0 * mc2 * beta ** 2 / (1 - beta ** 2)
    T = k * W ** alpha

    return T

def beta_energy(energy):

    E0 = 938.2  # proton rest mass MeV

    return np.sqrt(1 - (1 - energy / E0) ** 2)

def A(r, t, beta=0.01):
    t0 = 10e-15
    return np.exp(-(t-t0)**2/(2*t0**2))*radial_dose_distribution(r, beta)

def radial_dose_distribution(r, beta=0.01):
    """
    r - distance from path of an ion (radial distance) (cm)
    beta - ratio of v to c
    """
    conv = 1.6021e-13  # 1 Gy to KeV/g
    const = 8.5 / (2.0 * np.pi)  # KeV/cm 2piNe4/mc2

    alpha = 1.079  # alpha Fig. 1 key (a)
    mc2 = 510.998  # electron mass in KeV

    # effective charge of an ion (Eq. 10)
    z = 1  # proton
    zef = z * (1 - np.exp(-125.0 * beta * z ** (-2 / 3)))

    # the range of an electron (Eq. 5)
    k = 6.0e-6
    theta = k * (0.01) ** alpha

    # maximum range of delta-rays (Eq. 6-7)
    W = 2.0 * mc2 * beta ** 2 / (1 - beta ** 2)
    T = k * W ** alpha

    return (
        conv
        * (const * zef ** 2)
        / (r * alpha * beta ** 2)
        * ((1 - ((r + theta) / (T + theta))) ** (1 / alpha))
        / (r + theta)
    )


def main():
    x_nm = lambda x: x * 1.0e7  # convert xscale to nm

    beta = beta_energy(1)
    x = np.geomspace(1.0e-15, r_max(beta), 1000, dtype=np.float64)
    y_1mev = radial_dose_distribution(x, beta)
    plt.loglog(x_nm(x), y_1mev, label="1 MeV")

    beta = beta_energy(20)
    x = np.geomspace(1.0e-15, r_max(beta), 1000, dtype=np.float64)
    y_20mev = radial_dose_distribution(x, beta)
    plt.loglog(x_nm(x), y_20mev, label="20 MeV")

    beta = beta_energy(100)
    x = np.geomspace(1.0e-15, r_max(beta), 1000, dtype=np.float64)
    y_100mev = radial_dose_distribution(x, beta)
    plt.loglog(x_nm(x), y_100mev, label="100 MeV")

    plt.xlabel("Radius (nm)")
    plt.ylabel("Radial dose distribution (Gy)")
    plt.show()

if __name__ == "__main__":
    main()
