import numpy as np

def specific_heat(Te):
    kb = 8.6173324e-5  # [eV/K] Boltzmann constant
    na = 4.506 # [g/cm3] titanium
    ne = 2*na # [g/cm3] eletronic density
    me = 9.10938e-28 # [g] electron mass
    h = 4.135667e-15 # [eV/Hz] Plack constant

    Ef = (h/(2*np.pi))**2/(2*me)
    return Te*np.power(np.pi*kb, 2)*ne/(2*Ef)

def themal_conductiviy(T):
    pass
