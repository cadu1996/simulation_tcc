def specific_heat(T):
    """
    Lattice specific heat C[J g^-1 K^-1]
    """

    if 10 <= T <= 273:
        return 0.23

    if 273 < T <= 2125:
        return 0.28 - 0.00022 * T + 1.2 * (10 ** -7) * (T ** 2)

    if T > 2125:
        return 0.37

    return None


def themal_conductiviy(T):
    """
    Lattice thermal conductiviy K[W cm^-1 K^-1]
    """

    if 10 <= T <= 100:
        return (
            -0.078
            + 0.16 * T
            - 0.0061 * (T ** 2)
            + 7.8 * (10 ** -5) * (T ** 3)
            - 3.3 * (10 ** -7) * (T ** 4)
        )

    if 30 < T <= 2125:
        return (
            0.28
            - 0.00051 * T
            + 6.0 * (10 ** -7) * (T ** 2)
            - 1.7 * (10 ** -10) * (10 ** 3)
        )

    if T > 2125:
        return 0.34
